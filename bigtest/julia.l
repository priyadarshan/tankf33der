(seed (in "/dev/urandom" (rd 8)))
(de rnd NIL 
   (let Big (| (rand 0 7) (>> -28 (rand 0 15)) (>> -57 (rand 0 7)))
      (when (rand T)
         (setq Big (| Big `(hex "1FFFFFF0FFFFFF8"))) )
      (do (rand 0 2)
         (let Dig (| (rand 0 7) (>> -30 (rand 0 15)) (>> -61 (rand 0 7)))
            (when (rand T)
               (setq Dig (| Dig `(hex "1FFFFFFC3FFFFFF8"))) )
            (setq Big (| Dig (>> -64 Big))) ) )
      (if (rand T) Big (- Big)) ) )
(de testme @
   (let (N (read)  X (eval (rest)))
      (unless (= N X)
         (println 'N: N 'X: X 'rest: (rest))
         (bye) ) ) )
(de cmpge @
   (let (N (= "true" (read))  X (eval (rest)))
      (unless (= N X)
         (println 'N: N 'X: X 'rest: (rest))
         (bye) ) ) )
(de bigint (N1 S N2)
   (prinl "BigInt(" N1 ") " S " BigInt(" N2 ")") )
(de divJ (N1 N2)
   (prinl "div(" N1 ", " N2 ")") )
(de andJ (N1 N2)
   (prinl "abs(" N1 ") & abs(" N2 ")") )
(de orJ (N1 N2)
   (prinl "abs(" N1 ") | abs(" N2 ")") )
(de xorJ (N1 N2)
   (prinl "xor(abs(" N1 "), abs(" N2 "))") )
(de lshJ (N1 N2)
   (prinl "BigInt(abs(" N1 ")) << abs(" N2 ")") )
(de rshJ (N1 N2)
   (prinl "BigInt(abs(" N1 ")) >> abs(" N2 ")") )
(pipe
   (out '(julia)
      (do 10000
         (setq N1 (rnd))
         (while (=0 (setq N2 (rnd))))
         (prinl N1)
         (prinl N2)
         (bigint N1 "+" N2)
         (bigint N1 "+" 1)
         (bigint N1 "+" 2)
         (bigint N1 "+" 3)
         (bigint N1 "+" 4)
         (bigint N1 "+" 5)
         (bigint N1 "+" 6)
         (bigint N1 "+" 7)
         (bigint N1 "+" 8)
         (bigint N1 "+" 9)
         (bigint N1 "+" 10)
         (bigint N1 "-" N2)
         (bigint N1 "-" 1)
         (bigint N1 "-" 2)
         (bigint N1 "-" 3)
         (bigint N1 "-" 4)
         (bigint N1 "-" 5)
         (bigint N1 "-" 6)
         (bigint N1 "-" 7)
         (bigint N1 "-" 8)
         (bigint N1 "-" 9)
         (bigint N1 "-" 10)
         (bigint N1 "*" N2)
         (bigint N1 "*" 1)
         (bigint N1 "*" 2)
         (bigint N1 "*" 3)
         (bigint N1 "*" 4)
         (bigint N1 "*" 5)
         (bigint N1 "*" 6)
         (bigint N1 "*" 7)
         (bigint N1 "*" 8)
         (bigint N1 "*" 9)
         (bigint N1 "*" 10)
         (bigint N1 "%" N2)
         (divJ N1 N2)
         (divJ N1 1)
         (divJ N1 2)
         (divJ N1 3)
         (divJ N1 4)
         (divJ N1 5)
         (divJ N1 6)
         (divJ N1 7)
         (divJ N1 8)
         (divJ N1 9)
         (divJ N1 10)
         (andJ N1 N2)
         (andJ N1 1)
         (andJ N1 2)
         (andJ N1 3)
         (andJ N1 4)
         (andJ N1 5)
         (andJ N1 6)
         (andJ N1 7)
         (andJ N1 8)
         (andJ N1 9)
         (andJ N1 10)
         (orJ N1 N2)
         (orJ N1 1)
         (orJ N1 2)
         (orJ N1 3)
         (orJ N1 4)
         (orJ N1 5)
         (orJ N1 6)
         (orJ N1 7)
         (orJ N1 8)
         (orJ N1 9)
         (orJ N1 10)
         (xorJ N1 N2)
         (xorJ N1 1)
         (xorJ N1 2)
         (xorJ N1 3)
         (xorJ N1 4)
         (xorJ N1 5)
         (xorJ N1 6)
         (xorJ N1 7)
         (xorJ N1 8)
         (xorJ N1 9)
         (xorJ N1 10)
         (lshJ N1 1)
         (lshJ N1 2)
         (lshJ N1 3)
         (lshJ N1 4)
         (lshJ N1 5)
         (lshJ N1 6)
         (lshJ N1 7)
         (lshJ N1 8)
         (lshJ N1 9)
         (lshJ N1 10)
         (lshJ N1 1024)
         (rshJ N1 1)
         (rshJ N1 2)
         (rshJ N1 3)
         (rshJ N1 4)
         (rshJ N1 5)
         (rshJ N1 6)
         (rshJ N1 7)
         (rshJ N1 8)
         (rshJ N1 9)
         (rshJ N1 10)
         (rshJ N1 1024)
         (prinl N1 ">=" N2)
         (at (0 . 1000) (wait 50)) ) )
   (do 10
      (do 1000
         (setq N1 (read)  N2 (read)) 
         (testme '+ N1 N2)
         (testme '+ N1 1)
         (testme '+ N1 2)
         (testme '+ N1 3)
         (testme '+ N1 4)
         (testme '+ N1 5)
         (testme '+ N1 6)
         (testme '+ N1 7)
         (testme '+ N1 8)
         (testme '+ N1 9)
         (testme '+ N1 10)
         (testme '- N1 N2)
         (testme '- N1 1)
         (testme '- N1 2)
         (testme '- N1 3)
         (testme '- N1 4)
         (testme '- N1 5)
         (testme '- N1 6)
         (testme '- N1 7)
         (testme '- N1 8)
         (testme '- N1 9)
         (testme '- N1 10)
         (testme '* N1 N2)
         (testme '* N1 1)
         (testme '* N1 2)
         (testme '* N1 3)
         (testme '* N1 4)
         (testme '* N1 5)
         (testme '* N1 6)
         (testme '* N1 7)
         (testme '* N1 8)
         (testme '* N1 9)
         (testme '* N1 10)
         (testme '% N1 N2)
         (testme '/ N1 N2)
         (testme '/ N1 1)
         (testme '/ N1 2)
         (testme '/ N1 3)
         (testme '/ N1 4)
         (testme '/ N1 5)
         (testme '/ N1 6)
         (testme '/ N1 7)
         (testme '/ N1 8)
         (testme '/ N1 9)
         (testme '/ N1 10)
         (testme '& N1 N2)
         (testme '& N1 1)
         (testme '& N1 2)
         (testme '& N1 3)
         (testme '& N1 4)
         (testme '& N1 5)
         (testme '& N1 6)
         (testme '& N1 7)
         (testme '& N1 8)
         (testme '& N1 9)
         (testme '& N1 10)
         (testme '| N1 N2)
         (testme '| N1 1)
         (testme '| N1 2)
         (testme '| N1 3)
         (testme '| N1 4)
         (testme '| N1 5)
         (testme '| N1 6)
         (testme '| N1 7)
         (testme '| N1 8)
         (testme '| N1 9)
         (testme '| N1 10)
         (testme 'x| N1 N2)
         (testme 'x| N1 1)
         (testme 'x| N1 2)
         (testme 'x| N1 3)
         (testme 'x| N1 4)
         (testme 'x| N1 5)
         (testme 'x| N1 6)
         (testme 'x| N1 7)
         (testme 'x| N1 8)
         (testme 'x| N1 9)
         (testme 'x| N1 10)
         (testme '>> -1 (abs N1))
         (testme '>> -2 (abs N1))
         (testme '>> -3 (abs N1))
         (testme '>> -4 (abs N1))
         (testme '>> -5 (abs N1))
         (testme '>> -6 (abs N1))
         (testme '>> -7 (abs N1))
         (testme '>> -8 (abs N1))
         (testme '>> -9 (abs N1))
         (testme '>> -10 (abs N1))
         (testme '>> -1024 (abs N1))
         (testme '>> 1 (abs N1))
         (testme '>> 2 (abs N1))
         (testme '>> 3 (abs N1))
         (testme '>> 4 (abs N1))
         (testme '>> 5 (abs N1))
         (testme '>> 6 (abs N1))
         (testme '>> 7 (abs N1))
         (testme '>> 8 (abs N1))
         (testme '>> 9 (abs N1))
         (testme '>> 10 (abs N1))
         (testme '>> 1024 (abs N1))
         (cmpge '>= N1 N2) )
      (prin ".")
      (flush) ) )
(prinl)
      
(msg 'OK-julia)
(bye)
